# config-service [![Build Status](https://travis-ci.org/vivekkothari/config-service.svg?branch=master)](https://travis-ci.org/vivekkothari/config-service) [![Coverage Status](https://coveralls.io/repos/github/vivekkothari/config-service/badge.svg?branch=master)](https://coveralls.io/github/vivekkothari/config-service?branch=master)
A config management service.

## Usage
This service can be used to manage shared configuration which can be really helpful in micro service architecture.
The APIs have a really low latency (P95 of GETs ~2-3ms), and the http-client supports LRU caching of configs.

### Build instructions
  - Clone the source:

        git clone github.com/vivekkothari/config-service

  - Build

        mvn install
