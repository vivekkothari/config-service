package com.github.vivekothari.config.resource;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vivekothari.config.ConfigApplication;
import com.github.vivekothari.config.api.*;
import com.github.vivekothari.config.config.ConfigAppConfig;
import com.github.vivekothari.config.config.ESConfig;
import com.github.vivekothari.config.dao.ESSchemaDao;
import com.github.vivekothari.config.dao.RedisConfigDao;
import com.github.vivekothari.config.service.ConfigServiceImpl;
import com.github.vivekothari.config.service.IConfigService;
import com.github.vivekothari.config.service.SchemaServiceImpl;
import com.google.common.collect.Lists;
import io.dropwizard.jackson.Jackson;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.*;
import redis.clients.jedis.JedisPool;
import redis.embedded.RedisServer;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.github.vivekothari.config.resource.TestUtils.createIndex;
import static com.github.vivekothari.config.resource.TestUtils.metadata;

/**
 * @author vivek.kothari on 17/07/16.
 */
public class ConfigResourceTest {

    public static final String FILE_PATH = ResourceHelpers.resourceFilePath("test-config.yml");
    @ClassRule
    public static DropwizardAppRule<ConfigAppConfig> rule = new DropwizardAppRule<>(ConfigApplication.class, FILE_PATH);
    private static RedisServer redisServer;
    private static MockElasticsearchServer server;

    @Rule
    public ResourceTestRule resourceTestRule;

    private Client client;

    @BeforeClass
    public static void setupClass() throws Exception {
        redisServer = new RedisServer(6379);
        redisServer.start();

        final ESConfig esConfig = rule.getConfiguration()
                                      .getEsConfig();
        final String indexName = esConfig.getIndexName();
        server = new MockElasticsearchServer(esConfig.getClusterName(), Lists.newArrayList(indexName));
    }

    @AfterClass
    public static void teardownClass() throws InterruptedException, IOException {
        server.shutdown();
    }

    @Rule
    public ResourceTestRule resourceTestRule() {
        final SchemaServiceImpl schemaService = new SchemaServiceImpl(new ESSchemaDao(rule.getObjectMapper(), rule.getConfiguration()
                                                                                                                  .getEsConfig(), () -> server.getClient()));
        final IConfigService service = new ConfigServiceImpl(new RedisConfigDao(new JedisPool(), rule.getObjectMapper()), schemaService);
        resourceTestRule = ResourceTestRule.builder()
                                           .addResource(new ConfigResource(service))
                                           .addResource(new SchemaResource(schemaService))
                                           .setValidator(rule.getEnvironment()
                                                             .getValidator())
                                           .build();
        return resourceTestRule;
    }

    @Before
    public void setUp() throws Exception {
        client = resourceTestRule.client();
        createIndex(rule, server);
        redisServer.stop();
        redisServer.start();
    }

    @After
    public void tearDown() throws Exception {
        client.close();
        server.deleteIndex(rule.getConfiguration()
                               .getEsConfig()
                               .getIndexName());
        redisServer.stop();
    }

    @Test
    public void testStandardSchema() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}");
        final Response response = client.target("/v1/config/tenant/config/1")
                                        .request()
                                        .put(Entity.entity(new ExampleConfig("Vivek", "SDE"), MediaType.APPLICATION_JSON));
        Assert.assertEquals(201, response.getStatus());

        final ExampleConfig exampleConfig = client.target("/v1/config/tenant/config/1")
                                                  .request()
                                                  .get(ExampleConfig.class);
        Assert.assertNotNull(exampleConfig);
        Assert.assertEquals("Vivek", exampleConfig.getName());
        Assert.assertEquals("SDE", exampleConfig.getProfession());

        Assert.assertEquals(404, client.target("/v1/config/tenant/config/2")
                                       .request()
                                       .get()
                                       .getStatus());
    }

    @Test
    public void testStandardSchema_fail() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}");
        final Response response = client.target("/v1/config/tenant/config/1")
                                        .request()
                                        .put(Entity.entity("{\"profession\": \"SDE\"}", MediaType.APPLICATION_JSON));
        Assert.assertEquals(400, response.getStatus());
    }

    @Test
    public void testStandardSchema_invalid_schema() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}");
        final Response response = client.target("/v1/config/tenant/config/1")
                                        .request()
                                        .put(Entity.entity("{\"profession\": \"SDE\"}", MediaType.APPLICATION_JSON));
        Assert.assertEquals(400, response.getStatus());
        final Map<String, String> map = response.readEntity(Map.class);
        final ObjectMapper mapper = Jackson.newObjectMapper();
        final ValidationResponse error = mapper.convertValue(mapper.readTree(map.get("error")), ValidationResponse.class);
        Assert.assertEquals(ValidationStatus.VALIDATION_FAILED, error.getStatus());
    }

    @Test
    public void testKVSchema_invalid_schema() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}",
                metadata(Metadata.ConfigType.keyvalue));
        final Response response = client.target("/v1/config/tenant/config/1/id")
                                        .request()
                                        .put(Entity.entity("{\"profession\": \"SDE\"}", MediaType.APPLICATION_JSON));
        Assert.assertEquals(400, response.getStatus());
        final Map<String, String> map = response.readEntity(Map.class);
        final ObjectMapper mapper = Jackson.newObjectMapper();
        final ValidationResponse error = mapper.convertValue(mapper.readTree(map.get("error")), ValidationResponse.class);
        Assert.assertEquals(ValidationStatus.VALIDATION_FAILED, error.getStatus());
    }

    @Test
    public void testStandardSchema_fail_kv() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}",
                metadata(Metadata.ConfigType.keyvalue));
        final Response response = client.target("/v1/config/tenant/config/1")
                                        .request()
                                        .put(Entity.entity("{\"name\": \"Vivek\"}", MediaType.APPLICATION_JSON));
        Assert.assertEquals(400, response.getStatus());
    }

    @Test
    public void testKVSchema() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}",
                metadata(Metadata.ConfigType.keyvalue));
        Response response = client.target("/v1/config/tenant/config/1")
                                  .request()
                                  .put(Entity.entity(new ExampleConfig("Vivek", "SDE"), MediaType.APPLICATION_JSON));
        Assert.assertEquals(400, response.getStatus());

        response = client.target("/v1/config/tenant/config/1/id")
                         .request()
                         .put(Entity.entity(new ExampleConfig("Vivek", "SDE"), MediaType.APPLICATION_JSON));
        Assert.assertEquals(201, response.getStatus());

        final ExampleConfig exampleConfig = client.target("/v1/config/tenant/config/1/id")
                                                  .request()
                                                  .get(ExampleConfig.class);
        Assert.assertNotNull(exampleConfig);
        Assert.assertEquals("Vivek", exampleConfig.getName());
        Assert.assertEquals("SDE", exampleConfig.getProfession());
    }

    @Test
    public void testKVSchemaBulk() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}",
                metadata(Metadata.ConfigType.keyvalue));
        final ObjectMapper mapper = rule.getObjectMapper();
        final ExampleConfig config = new ExampleConfig("Vivek", "SDE");
        final List<BulkConfigRequest> list = Lists.newArrayList(new BulkConfigRequest("1", mapper.convertValue(config, JsonNode.class)),
                                                                new BulkConfigRequest("2", mapper.convertValue(config, JsonNode.class)));

        Response response = client.target("/v1/config/tenant/config/1/bulk")
                                  .request()
                                  .put(Entity.entity(list, MediaType.APPLICATION_JSON));
        Assert.assertEquals(201, response.getStatus());

        Assert.assertNotNull(client.target("/v1/config/tenant/config/1/1")
                                   .request()
                                   .get(ExampleConfig.class));

        Assert.assertNotNull(client.target("/v1/config/tenant/config/1/2")
                                   .request()
                                   .get(ExampleConfig.class));
    }

    private void createSchema(String jsonSchema) throws IOException {
        createSchema(jsonSchema, metadata());
    }

    private void createSchema(String jsonSchema, Metadata metadata) throws IOException {
        final ObjectMapper mapper = rule.getObjectMapper();
        final Schema schema = new Schema(metadata, mapper.readTree(jsonSchema));
        final Response response = client.target("/v1/schema")
                                        .request()
                                        .put(Entity.entity(schema, MediaType.APPLICATION_JSON));
        Assert.assertEquals(201, response.getStatus());
    }

}