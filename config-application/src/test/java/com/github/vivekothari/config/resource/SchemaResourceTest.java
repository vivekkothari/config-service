package com.github.vivekothari.config.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vivekothari.config.ConfigApplication;
import com.github.vivekothari.config.api.Metadata;
import com.github.vivekothari.config.api.Schema;
import com.github.vivekothari.config.bean.MetaResponse;
import com.github.vivekothari.config.config.ConfigAppConfig;
import com.github.vivekothari.config.config.ESConfig;
import com.github.vivekothari.config.dao.ESSchemaDao;
import com.github.vivekothari.config.service.SchemaServiceImpl;
import com.google.common.collect.Lists;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.*;
import redis.embedded.RedisServer;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

import static com.github.vivekothari.config.resource.TestUtils.createIndex;
import static com.github.vivekothari.config.resource.TestUtils.metadata;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author vivek.kothari on 17/07/16.
 */
public class SchemaResourceTest {

    public static final String FILE_PATH = ResourceHelpers.resourceFilePath("test-config.yml");
    @ClassRule
    public static DropwizardAppRule<ConfigAppConfig> rule = new DropwizardAppRule<>(ConfigApplication.class, FILE_PATH);
    private static RedisServer redisServer;
    private static MockElasticsearchServer server;

    @Rule
    public ResourceTestRule resourceTestRule;

    private Client client;

    @BeforeClass
    public static void setupClass() throws Exception {
        redisServer = new RedisServer(6379);

        final ESConfig esConfig = rule.getConfiguration()
                                      .getEsConfig();
        final String indexName = esConfig.getIndexName();
        server = new MockElasticsearchServer(esConfig.getClusterName(), Lists.newArrayList(indexName));
    }

    @AfterClass
    public static void teardownClass() throws InterruptedException, IOException {
        server.shutdown();
    }

    @Rule
    public ResourceTestRule resourceTestRule() {
        resourceTestRule = ResourceTestRule.builder()
                                           .addResource(new SchemaResource(new SchemaServiceImpl(new ESSchemaDao(rule.getObjectMapper(), rule.getConfiguration()
                                                                                                                                             .getEsConfig(),
                                                                                                                 () -> server.getClient()))))
                                           .setValidator(rule.getEnvironment()
                                                             .getValidator())
                                           .build();
        return resourceTestRule;
    }

    @Before
    public void setUp() throws Exception {
        client = resourceTestRule.client();
        createIndex(rule, server);
        redisServer.start();
    }

    @After
    public void tearDown() throws Exception {
        client.close();
        server.deleteIndex(rule.getConfiguration()
                               .getEsConfig()
                               .getIndexName());
        redisServer.stop();
    }

    @Test
    public void testGetAllTenants() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}");
        final Response response = client.target("/v1/schema/tenants")
                                        .request()
                                        .get();
        assertEquals(200, response.getStatus());
        final Object entity = response.getEntity();
        assertNotNull(entity);
        final List<String> list = response.readEntity(List.class);
        assertEquals(1, list.size());
        assertEquals("tenant", list.get(0));
    }

    @Test
    @Ignore
    public void testGetAllTenants_error() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}");
        server.deleteIndex(rule.getConfiguration()
                               .getEsConfig()
                               .getIndexName());
        final Response response = client.target("/v1/schema/tenants")
                                        .request()
                                        .get();
        assertEquals(500, response.getStatus());
    }

    private void createSchema(String jsonSchema) throws IOException {
        final ObjectMapper mapper = rule.getObjectMapper();
        final Schema schema = new Schema(metadata(), mapper.readTree(jsonSchema));
        client.target("/v1/schema")
              .request()
              .put(Entity.entity(schema, MediaType.APPLICATION_JSON));
    }

    @Test
    public void testGetTenantSchemaVersions() throws IOException {
        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\"]}");

        Response response = client.target("/v1/schema/tenant/config")
                                  .request()
                                  .get();
        assertEquals(200, response.getStatus());
        MetaResponse metaResponse = response.readEntity(MetaResponse.class);
        assertNotNull(metaResponse);
        assertEquals(1, metaResponse.getResult()
                                    .size());

        createSchema(
                "{\"$schema\":\"http://json-schema.org/draft-04/schema#\",\"type\":\"object\",\"properties\":{\"name\":{\"type\":\"string\"}," +
                        "\"profession\":{\"type\":\"string\"}},\"required\":[\"name\", \"profession\"]}");

        response = client.target("/v1/schema/tenant")
                         .request()
                         .get();
        assertEquals(200, response.getStatus());
        metaResponse = response.readEntity(MetaResponse.class);
        assertNotNull(metaResponse);
        List<Metadata> result = metaResponse.getResult();
        assertEquals(2, result.size());

        response = client.target("/v1/schema/tenant/config")
                         .request()
                         .get();
        assertEquals(200, response.getStatus());
        metaResponse = response.readEntity(MetaResponse.class);
        assertNotNull(metaResponse);
        result = metaResponse.getResult();
        assertEquals(2, result.size());
        assertEquals(true, result.stream()
                                 .map(Metadata::getVersion)
                                 .filter(i -> i == 1)
                                 .findFirst()
                                 .isPresent());
        assertEquals(true, result.stream()
                                 .map(Metadata::getVersion)
                                 .filter(i -> i == 2)
                                 .findFirst()
                                 .isPresent());

        Schema schema = client.target("/v1/schema/tenant/config/latest")
                              .request()
                              .get(Schema.class);
        assertNotNull(schema);
        assertEquals(true, schema.getMetadata()
                                 .isLatest());
        assertEquals((int) 2, (int) schema.getMetadata()
                                          .getVersion());

        schema = client.target("/v1/schema/tenant/config/1")
                       .request()
                       .get(Schema.class);
        assertNotNull(schema);
        assertEquals(false, schema.getMetadata()
                                  .isLatest());
        assertEquals((int) 1, (int) schema.getMetadata()
                                          .getVersion());

        final int status = client.target("/v1/schema/tenant/config/5")
                                 .request()
                                 .get()
                                 .getStatus();
        assertEquals(404, status);
    }

    @Test
    public void testGetTenantSchemaVersions_failure() throws IOException {
        Response response = client.target("/v1/schema/tenant/config/latest")
                                  .request()
                                  .get();
        assertEquals(404, response.getStatus());

        server.deleteIndex(rule.getConfiguration()
                               .getEsConfig()
                               .getIndexName());
        response = client.target("/v1/schema/tenant/config/latest")
                         .request()
                         .get();
        assertEquals(500, response.getStatus());
    }

}