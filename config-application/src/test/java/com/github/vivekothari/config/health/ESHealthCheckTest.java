package com.github.vivekothari.config.health;

import com.codahale.metrics.health.HealthCheck;
import com.github.vivekothari.config.ConfigApplication;
import com.github.vivekothari.config.config.ConfigAppConfig;
import com.github.vivekothari.config.config.ESConfig;
import com.github.vivekothari.config.resource.MockElasticsearchServer;
import com.google.common.collect.Lists;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.*;

import java.io.IOException;

import static com.github.vivekothari.config.resource.TestUtils.createIndex;

/**
 * @author vivek.kothari on 18/07/16.
 */
public class ESHealthCheckTest {

    public static final String FILE_PATH = ResourceHelpers.resourceFilePath("test-config.yml");
    @ClassRule
    public static DropwizardAppRule<ConfigAppConfig> rule = new DropwizardAppRule<>(ConfigApplication.class, FILE_PATH);
    private static MockElasticsearchServer server1;
    private static MockElasticsearchServer server2;
    private ESHealthCheck healthCheck;

    @BeforeClass
    public static void setupClass() throws Exception {
        final ESConfig esConfig = rule.getConfiguration()
                                      .getEsConfig();
        final String indexName = esConfig.getIndexName();
        server1 = new MockElasticsearchServer(esConfig.getClusterName(), Lists.newArrayList(indexName));
        server2 = new MockElasticsearchServer(esConfig.getClusterName(), Lists.newArrayList(indexName));
    }

    @AfterClass
    public static void teardownClass() throws InterruptedException, IOException {
        server1.shutdown();
        server2.shutdown();
    }

    @Before
    public void setUp() throws Exception {
        createIndex(rule, server1);
        healthCheck = new ESHealthCheck(() -> server1.getClient(), rule.getConfiguration()
                                                                       .getEsConfig());
    }

    @After
    public void tearDown() throws Exception {
        server1.deleteIndex(rule.getConfiguration()
                                .getEsConfig()
                                .getIndexName());
        server2.deleteIndex(rule.getConfiguration()
                                .getEsConfig()
                                .getIndexName());
    }

    @Test
    public void check() throws Exception {
        final HealthCheck.Result result = healthCheck.check();
        Assert.assertEquals(true, result.isHealthy());
    }

    @Test
    public void check_yellow() throws Exception {
        server2.deleteIndex(rule.getConfiguration()
                                .getEsConfig()
                                .getIndexName());
        final HealthCheck.Result result = healthCheck.check();
        Assert.assertEquals(false, result.isHealthy());
    }

}