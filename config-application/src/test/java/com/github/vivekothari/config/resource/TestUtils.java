package com.github.vivekothari.config.resource;

import com.github.vivekothari.config.api.Metadata;
import com.github.vivekothari.config.config.ConfigAppConfig;
import com.github.vivekothari.config.config.ESConfig;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.common.settings.Settings;

/**
 * @author vivek.kothari on 17/07/16.
 */
public class TestUtils {

    public static void createIndex(DropwizardAppRule<ConfigAppConfig> rule, MockElasticsearchServer server) {
        final ESConfig esConfig = rule.getConfiguration()
                                      .getEsConfig();
        final String indexName = esConfig.getIndexName();
        final org.elasticsearch.client.Client client = server.getClient();
        final CreateIndexResponse createIndexResponse = client.admin()
                                                              .indices()
                                                              .prepareCreate(indexName)
                                                              .setSettings(Settings.builder()
                                                                                   .put("index.number_of_shards", 1)
                                                                                   .put("index.routing.allocation.disable_allocation", false)
                                                                                   .put("index.number_of_replicas", 1))
                                                              .addMapping(esConfig.getIndexType(), "{\"properties\":{\"schema\":{\"type\":\"object\",\"enabled\":false}," +
                                                                      "\"metadata\":{\"properties\":{\"id\":{\"type\":\"string\"},\"createdAt\":{\"type\":\"long\"}," +
                                                                      "\"latest\":{\"type\":\"boolean\"}," +
                                                                      "\"configName\":{\"type\":\"string\",\"index\":\"not_analyzed\",\"doc_values\":true," +
                                                                      "\"copy_to\":[\"search\"]},\"tenant\":{\"type\":\"string\"," +
                                                                      "\"index\":\"not_analyzed\",\"doc_values\":true,\"copy_to\":[\"search\"]},\"version\":{\"type\":\"long\"}," +
                                                                      "\"search\":{\"type\":\"string\"}," +
                                                                      "\"configType\":{\"type\":\"string\"}}},\"search\":{\"type\":\"string\"}}}")
                                                              .get();
        server.refresh(indexName);
    }

    public static Metadata metadata() {
        return metadata(Metadata.ConfigType.standard);
    }

    public static Metadata metadata(Metadata.ConfigType configType) {
        return Metadata.builder()
                       .configName("config")
                       .configType(configType)
                       .tenant("tenant")
                       .build();
    }

}
