package com.github.vivekothari.config.service;

import com.github.vivekothari.config.api.Schema;
import com.github.vivekothari.config.bean.MetaResponse;
import com.github.vivekothari.config.bean.SortBy;

import java.io.IOException;
import java.util.List;

/**
 * @author vivek.kothari on 17/07/16.
 */
public interface ISchemaService {

    List<String> fetchAllTenants();

    Schema createSchema(final Schema schema) throws IOException;

    Schema fetch(final String tenant, final String name, final int version) throws IOException;

    Schema fetchLatest(final String tenant, final String configName) throws IOException;

    MetaResponse fetchMeta(final String tenant, final Integer from, final Integer size, final SortBy sortBy) throws IOException;

    MetaResponse fetchMeta(final String tenant, final String configName, final Integer from, final Integer size, final SortBy sortBy) throws IOException;
}
