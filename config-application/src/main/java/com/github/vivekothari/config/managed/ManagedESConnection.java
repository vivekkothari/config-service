package com.github.vivekothari.config.managed;

import com.github.vivekothari.config.config.ESConfig;
import io.dropwizard.lifecycle.Managed;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.net.InetSocketAddress;
import java.util.Objects;
import java.util.function.Supplier;

/**
 * @author vivek.kothari on 22/03/16.
 */
@Slf4j
public class ManagedESConnection
        implements Managed, Supplier<Client> {

    private final ESConfig esConfig;
    private Client client;

    public ManagedESConnection(final ESConfig esConfig) {
        this.esConfig = esConfig;
    }

    @Override
    public void start() throws Exception {
        log.info("Starting Elasticsearch Client Connection...");
        final Settings settings = Settings.builder()
                                          .put("cluster.name", esConfig.getClusterName())
                                          .build();
        final TransportClient build = TransportClient.builder()
                                                     .settings(settings)
                                                     .build();
        for (final String host : esConfig.getHosts()) {
            build.addTransportAddress(new InetSocketTransportAddress(new InetSocketAddress(host.trim(), 9300)));
            log.info("Added Elasticsearch Node : {}", host);
        }
        this.client = build;
        log.info("Started Elasticsearch Client Connection...");
    }

    @Override
    public void stop() throws Exception {
        client.close();
        log.info("Closed Elasticsearch Client Connection...");
    }

    @Override
    public Client get() {
        return Objects.requireNonNull(client, "Client not initialized yet.");
    }
}
