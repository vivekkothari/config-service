package com.github.vivekothari.config.bean;

import com.github.vivekothari.config.api.Metadata;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author vivek.kothari
 */
@Getter
@Setter
@AllArgsConstructor
public class MetaResponse {
    private List<Metadata> result;
    private PagingParam pagingParam;
}
