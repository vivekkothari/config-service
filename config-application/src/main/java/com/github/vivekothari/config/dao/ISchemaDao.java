package com.github.vivekothari.config.dao;

import com.github.vivekothari.config.api.Schema;
import com.github.vivekothari.config.bean.MetaResponse;
import com.github.vivekothari.config.bean.SortBy;

import java.io.IOException;
import java.util.List;

/**
 * @author vivek.kothari on 17/07/16.
 */
public interface ISchemaDao {

    List<String> fetchAllTenants();

    Schema persist(Schema schema);

    Schema fetchSchema(String tenant, String name, int version) throws IOException;

    Schema getLatestSchema(String tenant, String configName) throws IOException;

    MetaResponse fetchMeta(String tenant, Integer from, Integer size, SortBy sortBy) throws IOException;

    MetaResponse fetchMeta(String tenant, String configName, Integer from, Integer size, SortBy sortBy) throws IOException;
}
