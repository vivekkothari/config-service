package com.github.vivekothari.config.resource;

import com.codahale.metrics.annotation.Timed;
import com.github.vivekothari.config.api.Schema;
import com.github.vivekothari.config.bean.MetaResponse;
import com.github.vivekothari.config.bean.SortBy;
import com.github.vivekothari.config.service.ISchemaService;
import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

/**
 * @author vivek.kothari on 22/03/16.
 */
@Slf4j
@Consumes(MediaType.APPLICATION_JSON)
@Produces(value = {MediaType.APPLICATION_JSON})
@Path("/v1/schema")
@Api("/v1/schema")
public class SchemaResource {

    private final ISchemaService schemaService;

    public SchemaResource(final ISchemaService schemaService) {
        this.schemaService = schemaService;
    }

    @GET
    @Timed
    @Path("{tenant}")
    public Response getTenantSchemas(@PathParam("tenant") final String tenant, @QueryParam("from") final Integer from, @QueryParam("size") final Integer size,
                                     @QueryParam("sortBy") final SortBy sortBy) throws IOException {
        final MetaResponse result = this.schemaService.fetchMeta(tenant, from, size, sortBy);
        return Response.ok()
                       .entity(result)
                       .build();
    }

    @GET
    @Timed
    @Path("{tenant}/{configName}")
    public Response getTenantSchemaVersions(@PathParam("tenant") final String tenant, @PathParam("configName") final String configName, @QueryParam("from") final Integer from,
                                            @QueryParam("size") final Integer size, @QueryParam("sortBy") final SortBy sortBy) throws IOException {
        final MetaResponse result = this.schemaService.fetchMeta(tenant, configName, from, size, sortBy);
        return Response.ok()
                       .entity(result)
                       .build();

    }

    @GET
    @Timed
    @Path("tenants")
    public Response getAllTenants() {
        try {
            return Response.ok()
                           .entity(this.schemaService.fetchAllTenants())
                           .build();
        } catch (final Exception e) {
            log.error("Error - {}", e.getMessage(), e);
            return Response.serverError()
                           .entity(ImmutableMap.of("error", e.getMessage()))
                           .build();
        }
    }

    @PUT
    @Timed
    @ApiOperation("Creates a schema")
    public Response createSchema(@Valid Schema schema) throws IOException {
        return Response.status(Response.Status.CREATED)
                       .entity(schemaService.createSchema(schema))
                       .build();
    }

    @GET
    @Timed
    @Path("{tenant}/{configName}/{version}")
    @ApiOperation("Returns a schema")
    public Response getSchema(@PathParam("tenant") final String tenant, @PathParam("configName") final String configName, @PathParam("version") final int version)
            throws IOException {
        try {
            final Schema result = this.schemaService.fetch(tenant, configName, version);
            return Response.ok()
                           .entity(result)
                           .build();
        } catch (final NotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND)
                           .entity(ImmutableMap.of("error", e.getMessage()))
                           .build();
        }
    }

    @GET
    @Timed
    @Path("{tenant}/{configName}/latest")
    @ApiOperation("Retrieve latest schema")
    public Response getLatestSchema(@PathParam("tenant") final String tenant, @PathParam("configName") final String configName) throws IOException {
        try {
            final Schema schema = this.schemaService.fetchLatest(tenant, configName);
            return Response.ok()
                           .entity(schema)
                           .build();
        } catch (final NotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND)
                           .entity(ImmutableMap.of("error", e.getMessage()))
                           .build();
        }
    }

}
