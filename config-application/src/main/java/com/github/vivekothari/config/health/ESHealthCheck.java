package com.github.vivekothari.config.health;

import com.codahale.metrics.health.HealthCheck;
import com.github.vivekothari.config.config.ESConfig;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.unit.TimeValue;

import java.util.function.Supplier;

/**
 * Healthcheck for Elastic search
 *
 * @author vivek.kothari
 */
public class ESHealthCheck
        extends HealthCheck {

    private final Supplier<Client> clientSupplier;
    private final ESConfig config;

    public ESHealthCheck(final Supplier<Client> clientSupplier, final ESConfig config) {
        this.clientSupplier = clientSupplier;
        this.config = config;
    }

    /**
     * Here it checks the elastic-search health if cluster health is green
     */
    @Override
    protected Result check() throws Exception {
        final ClusterHealthResponse clusterHealth = clientSupplier.get()
                                                                  .admin()
                                                                  .cluster()
                                                                  .prepareHealth(config.getIndexName())
                                                                  .setWaitForGreenStatus()
                                                                  .get();
        return !clusterHealth.isTimedOut() ? Result.healthy() : Result.unhealthy("Elasticsearch is down");
    }
}
