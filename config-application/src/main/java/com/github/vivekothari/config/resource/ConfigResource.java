package com.github.vivekothari.config.resource;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.JsonNode;
import com.github.vivekothari.config.api.BulkConfigRequest;
import com.github.vivekothari.config.service.IConfigService;
import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ws.rs.core.Response.Status.*;
import static javax.ws.rs.core.Response.serverError;
import static javax.ws.rs.core.Response.status;

/**
 * @author vivek.kothari on 17/07/16.
 */
@Slf4j
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/v1/config")
@Api("/v1/config")
public class ConfigResource {

    private final IConfigService configService;

    public ConfigResource(final IConfigService configService) {
        this.configService = configService;
    }

    /**
     * Create standard config.
     *
     * @param configName
     * @param version
     * @param jsonNode
     *
     * @return
     */
    @PUT
    @Timed
    @Path("{tenant}/{configName}/{version}")
    @ApiOperation("Creates a new config.")
    @SneakyThrows
    public Response create(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                           @NotNull @PathParam("version") final int version, @NotNull @Valid final JsonNode jsonNode) {
        try {
            return Response.status(CREATED)
                           .entity(configService.createStandardConfig(tenant, configName, version, jsonNode))
                           .build();
        } catch (final BadRequestException | IllegalArgumentException e) {
            log.error("Error in create", e);
            return status(Response.Status.BAD_REQUEST).entity(ImmutableMap.of("error", e.getMessage()))
                                                      .build();
        }
    }

    /**
     * Get standard config.
     *
     * @param configName
     * @param version
     *
     * @return
     */
    @GET
    @Timed
    @Path("{tenant}/{configName}/{version}")
    @ApiOperation("Returns the config.")
    public Response getConfig(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                              @NotNull @PathParam("version") final int version) {
        try {
            return Response.status(OK)
                           .entity(configService.getStandardConfig(tenant, configName, version))
                           .build();
        } catch (NotFoundException e) {
            return status(NOT_FOUND).entity(ImmutableMap.of("error", "Config not found."))
                                    .build();
        } catch (final BadRequestException | IllegalArgumentException e) {
            log.error("Error in create", e);
            return status(Response.Status.BAD_REQUEST).entity(ImmutableMap.of("error", e.getMessage()))
                                                      .build();
        }
    }

    /**
     * Create KV config.
     *
     * @param configName
     * @param version
     * @param id
     * @param jsonNode
     *
     * @return
     */
    @PUT
    @Timed
    @Path("{tenant}/{configName}/{version}/{id}")
    @ApiOperation("Creates a new KV config.")
    @SneakyThrows
    public Response createKV(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                             @NotNull @PathParam("version") final int version, @NotNull @PathParam("id") final String id, @NotNull @Valid final JsonNode jsonNode) {
        try {
            return Response.status(CREATED)
                           .entity(configService.createKVConfig(tenant, configName, version, id, jsonNode))
                           .build();
        } catch (final BadRequestException | IllegalArgumentException e) {
            log.error("Error in create", e);
            return status(Response.Status.BAD_REQUEST).entity(ImmutableMap.of("error", e.getMessage()))
                                                      .build();
        }
    }

    /**
     * Get KV config.
     *
     * @param configName
     * @param version
     * @param id
     *
     * @return
     */
    @GET
    @Timed
    @Path("{tenant}/{configName}/{version}/{id}")
    @ApiOperation("Returns the KV config.")
    public Response getConfigKV(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                                @NotNull @PathParam("version") final int version, @NotNull @PathParam("id") final String id) {
        try {
            return Response.status(OK)
                           .entity(configService.getKVConfig(tenant, configName, version, id))
                           .build();
        } catch (NotFoundException e) {
            return status(NOT_FOUND).entity(ImmutableMap.of("error", "Config not found."))
                                    .build();
        } catch (final BadRequestException | IllegalArgumentException e) {
            log.error("Error in create", e);
            return status(Response.Status.BAD_REQUEST).entity(ImmutableMap.of("error", e.getMessage()))
                                                      .build();
        }
    }

    /**
     * Create KV config in bulk.
     *
     * @param configName
     * @param version
     * @param bulkConfigRequests
     *
     * @return
     */
    @PUT
    @Timed
    @Path("{tenant}/{configName}/{version}/bulk")
    @ApiOperation("Creates KV config in bulk config.")
    @SneakyThrows
    public Response createBulk(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                               @NotNull @PathParam("version") final int version, @Valid final List<BulkConfigRequest> bulkConfigRequests) {
        try {
            return Response.status(CREATED)
                           .entity(configService.createKVConfig(tenant, configName, version, bulkConfigRequests))
                           .build();
        } catch (final BadRequestException | IllegalArgumentException e) {
            log.error("Error in create", e);
            return status(Response.Status.BAD_REQUEST).entity(ImmutableMap.of("error", e.getMessage()))
                                                      .build();
        }
    }
}
