package com.github.vivekothari.config.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * @author vivek.kothari
 */
@Getter
@AllArgsConstructor
public class PagingParam {
    private int start;
    private int size;
    private long totalCount;
}
