package com.github.vivekothari.config.config;

import lombok.Data;

import java.util.List;

/**
 * @author vivek.kothari on 22/03/16.
 */
@Data
public class ESConfig {
    private String clusterName;
    private List<String> hosts;
    private String indexName;
    private String indexType;
}
