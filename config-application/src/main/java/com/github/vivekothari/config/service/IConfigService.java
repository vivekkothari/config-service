package com.github.vivekothari.config.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.vivekothari.config.api.BulkConfigRequest;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author vivek.kothari on 17/07/16.
 */
public interface IConfigService {

    JsonNode createStandardConfig(final String tenant, final String configName, final int version, final JsonNode data) throws IOException;

    Optional<JsonNode> getStandardConfig(final String tenant, final String configName, final int version);

    Map<String, JsonNode> createKVConfig(final String tenant, final String configName, final int version, final String id, final JsonNode data) throws IOException;

    Map<String, JsonNode> createKVConfig(final String tenant, final String configName, final int version, final List<BulkConfigRequest> configRequests) throws IOException;

    Optional<JsonNode> getKVConfig(final String tenant, final String configName, final int version, final String id);
}
