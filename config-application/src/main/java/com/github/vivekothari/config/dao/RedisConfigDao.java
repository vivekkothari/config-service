package com.github.vivekothari.config.dao;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toMap;

/**
 * @author vivek.kothari on 17/07/16.
 */
public class RedisConfigDao
        implements IConfigDao {

    private final JedisPool jedisPool;
    private final ObjectMapper objectMapper;

    public RedisConfigDao(final JedisPool jedisPool, final ObjectMapper objectMapper) {
        this.jedisPool = jedisPool;
        this.objectMapper = objectMapper;
    }

    @Override
    public JsonNode createStandardSchema(final String tenant, final String configName, final int version, final JsonNode data) {
        final String key = generateConfigKey(tenant, configName, version);
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(key, writeAsString(data));
        }
        return data;
    }


    @Override
    @SneakyThrows
    public Optional<JsonNode> getStandardConfig(final String tenant, final String configName, final int version) {
        final String key = generateConfigKey(tenant, configName, version);
        try (Jedis jedis = jedisPool.getResource()) {
            final Optional<String> optional = Optional.ofNullable(jedis.get(key));
            if (optional.isPresent()) {
                return Optional.of(objectMapper.readTree(optional.get()));
            }
        }
        return Optional.empty();
    }

    @Override
    public Map<String, JsonNode> createKVSchema(final String tenant, final String configName, final int version, final Map<String, JsonNode> data) {
        final String key = generateConfigKey(tenant, configName, version);
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.hmset(key, transform(data));
        }
        return data;
    }

    @Override
    @SneakyThrows
    public Optional<JsonNode> getKVConfig(final String tenant, final String configName, final int version, final String id) {
        final String key = generateConfigKey(tenant, configName, version);
        try (Jedis jedis = jedisPool.getResource()) {
            final Optional<String> optional = Optional.ofNullable(jedis.hget(key, id));
            if (optional.isPresent()) {
                return Optional.of(objectMapper.readTree(optional.get()));
            }
        }
        return Optional.empty();
    }

    String generateConfigKey(final String tenant, final String configName, final int version) {
        return String.format("%s.%s.%d", tenant, configName, version);
    }

    @SneakyThrows
    <V> String writeAsString(V value) {
        return objectMapper.writeValueAsString(value);
    }

    <K, V> Map<String, String> transform(Map<K, V> map) {
        return map.entrySet()
                  .stream()
                  .collect(toMap(e -> e.getKey()
                                       .toString(), e -> writeAsString(e.getValue())));
    }
}
