package com.github.vivekothari.config.service;

import com.github.vivekothari.config.JsonValidator;
import com.github.vivekothari.config.api.Schema;
import com.github.vivekothari.config.bean.MetaResponse;
import com.github.vivekothari.config.bean.SortBy;
import com.github.vivekothari.config.dao.ISchemaDao;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.NotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * {@link ISchemaService} implementation
 *
 * @author vivek.kothari on 17/07/16.
 */
@Slf4j
public class SchemaServiceImpl
        implements ISchemaService {

    private final ISchemaDao dao;

    public SchemaServiceImpl(final ISchemaDao dao) {
        this.dao = dao;
    }

    @Override
    public List<String> fetchAllTenants() {
        return dao.fetchAllTenants();
    }

    @Override
    public Schema createSchema(final Schema schema) throws IOException {
        final boolean isValid = JsonValidator.INSTANCE.isValidJSONSchema(schema.getSchema());
        if (!isValid) {
            log.error("Not a valid json schema");
            throw new RuntimeException("Invalid json schema for data");
        }
        try {
            final Schema response = this.dao.persist(schema);
            if (response == null) {
                log.error("No Change found in schema...");
                throw new RuntimeException("No Change found in schema...");
            }
            return response;
        } catch (final Exception e) {
            log.error("Unable to persist schema - {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Query by id
     *
     * @param name
     *         of schema to fetch
     * @param version
     *         of schema to fetch
     *
     * @return schema
     */
    @Override
    public Schema fetch(final String tenant, final String name, final int version) throws IOException {
        final Schema schema = dao.fetchSchema(tenant, name, version);
        if (schema == null) {
            log.error("No schema found for tenant {}, with name {}, version {} ", tenant, name, version);
            throw new NotFoundException("No schema found with tenant " + tenant + " - configName - " + name + ", version - " + version);
        }
        return schema;
    }

    /**
     * Getting latest of a schema
     *
     * @param configName
     *         to get latest
     *
     * @return schema
     */
    @Override
    public Schema fetchLatest(final String tenant, final String configName) throws IOException {
        final Schema schema = dao.getLatestSchema(tenant, configName);
        if (schema == null) {
            log.error("No schema found for tenant {} with name {} ", tenant, configName);
            throw new NotFoundException("No schema found with name - " + configName);
        }
        return schema;
    }

    /**
     * Fetch meta from tenant
     *
     * @param tenant
     *         to fetch schema
     * @param from
     *         to fetch schema
     * @param size
     *         to fetch schema
     * @param sortBy
     *         to fetch schema
     *
     * @return meta response
     *
     * @throws IOException
     */
    @Override
    public MetaResponse fetchMeta(final String tenant, final Integer from, final Integer size, final SortBy sortBy) throws IOException {
        return dao.fetchMeta(tenant, from, size, sortBy);
    }

    @Override
    public MetaResponse fetchMeta(final String tenant, final String configName, final Integer from, final Integer size, final SortBy sortBy) throws IOException {
        return dao.fetchMeta(tenant, configName, from, size, sortBy);
    }

}
