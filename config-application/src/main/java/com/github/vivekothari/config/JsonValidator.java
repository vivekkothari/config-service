package com.github.vivekothari.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.vivekothari.config.api.ValidationResponse;
import com.github.vivekothari.config.api.ValidationStatus;
import io.dropwizard.jackson.Jackson;
import lombok.extern.slf4j.Slf4j;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Collections;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @author vivek.kothari on 17/07/16.
 */
@Slf4j
public class JsonValidator {

    private static final ObjectMapper mapper = Jackson.newObjectMapper();
    private static final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
    public static JsonValidator INSTANCE = new JsonValidator();

    /**
     * Constructor
     */
    private JsonValidator() {
    }

    /**
     * Used for validating given json if it is valid avro format or not
     *
     * @param data
     *         to validate
     *
     * @return true if valid
     */
    public boolean isValidJSONSchema(final JsonNode schema) {
        return schema == null ? false : factory.getSyntaxValidator()
                                               .validateSchema(schema)
                                               .isSuccess();
    }

    /**
     * Validate given event against given schema
     *
     * @param schemaData
     *         against validate
     * @param config
     *         to validate
     *
     * @return ValidationResponse
     *
     * @throws JsonProcessingException
     */
    public ValidationResponse validateEvent(final JsonNode schemaData, final JsonNode config) throws JsonProcessingException {
        try {
            final Schema schema = SchemaLoader.load(new JSONObject(new JSONTokener(mapper.writeValueAsString(schemaData))));
            schema.validate(new JSONObject(new JSONTokener(mapper.writeValueAsString(config))));
        } catch (final ValidationException e) {
            log.error("Error - {}", e.getMessage(), e);
            return new ValidationResponse(ValidationStatus.VALIDATION_FAILED, newArrayList(e.getMessage()));
        } catch (final JSONException e) {
            log.error("Error - {}", e.getMessage(), e);
            return new ValidationResponse(ValidationStatus.JSON_PROCESSING_EXCEPTION, newArrayList(e.getMessage()));
        } catch (final Exception e) {
            log.error("Error - {}", e.getMessage(), e);
            return new ValidationResponse(ValidationStatus.OTHER_EXCEPTION, newArrayList(e.getMessage()));
        }
        return new ValidationResponse(ValidationStatus.SUCCESS, Collections.emptyList());
    }

}
