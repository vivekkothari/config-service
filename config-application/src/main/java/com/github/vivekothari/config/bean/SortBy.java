package com.github.vivekothari.config.bean;

/**
 * @author vivek.kothari
 */
public enum SortBy {
    asc, desc
}
