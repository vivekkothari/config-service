package com.github.vivekothari.config.dao;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Map;
import java.util.Optional;

/**
 * @author vivek.kothari on 17/07/16.
 */
public interface IConfigDao {

    JsonNode createStandardSchema(String tenant, String configName, int version, JsonNode data);

    Optional<JsonNode> getStandardConfig(String tenant, String configName, int version);

    Map<String, JsonNode> createKVSchema(String tenant, String configName, int version, Map<String, JsonNode> data);

    Optional<JsonNode> getKVConfig(String tenant, String configName, int version, String id);

}
