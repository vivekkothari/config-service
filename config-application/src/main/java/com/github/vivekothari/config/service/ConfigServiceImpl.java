package com.github.vivekothari.config.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.vivekothari.config.JsonValidator;
import com.github.vivekothari.config.api.*;
import com.github.vivekothari.config.dao.IConfigDao;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toMap;

/**
 * @author vivek.kothari on 17/07/16.
 */
public class ConfigServiceImpl
        implements IConfigService {

    private final IConfigDao configDao;
    private final ISchemaService schemaService;

    public ConfigServiceImpl(final IConfigDao configDao, final ISchemaService schemaService) {
        this.configDao = configDao;
        this.schemaService = schemaService;
    }

    @Override
    public JsonNode createStandardConfig(final String tenant, final String configName, final int version, final JsonNode data) throws IOException {
        final Schema schema = schemaService.fetch(tenant, configName, version);
        final Metadata metadata = schema.getMetadata();

        if (metadata.getConfigType() != Metadata.ConfigType.standard) {
            throw new IllegalArgumentException("The schema is not of standard type.");
        }

        final ValidationResponse response = JsonValidator.INSTANCE.validateEvent(schema.getSchema(), data);

        if (response.getStatus() == ValidationStatus.SUCCESS) {
            this.configDao.createStandardSchema(tenant, configName, version, data);
            return data;
        }
        throw new IllegalArgumentException(response.toString());
    }

    @Override
    public Optional<JsonNode> getStandardConfig(final String tenant, final String configName, final int version) {
        return configDao.getStandardConfig(tenant, configName, version);
    }

    @Override
    public Map<String, JsonNode> createKVConfig(final String tenant, final String configName, final int version, final String id, final JsonNode data) throws IOException {
        return createKVConfig(tenant, configName, version, newArrayList(new BulkConfigRequest(id, data)));
    }

    @Override
    public Map<String, JsonNode> createKVConfig(final String tenant, final String configName, final int version, final List<BulkConfigRequest> configRequests) throws IOException {
        final Map<String, JsonNode> data = configRequests.stream()
                                                         .collect(toMap(BulkConfigRequest::getId, BulkConfigRequest::getData));
        final Schema schema = schemaService.fetch(tenant, configName, version);
        final Metadata metadata = schema.getMetadata();

        if (metadata.getConfigType() != Metadata.ConfigType.keyvalue) {
            throw new IllegalArgumentException("The schema is not of keyvalue type.");
        }

        for (final JsonNode node : data.values()) {
            final ValidationResponse response = JsonValidator.INSTANCE.validateEvent(schema.getSchema(), node);
            if (response.getStatus() != ValidationStatus.SUCCESS) {
                throw new IllegalArgumentException(response.toString());
            }
        }
        this.configDao.createKVSchema(tenant, configName, version, data);
        return data;
    }

    @Override
    public Optional<JsonNode> getKVConfig(final String tenant, final String configName, final int version, final String id) {
        return configDao.getKVConfig(tenant, configName, version, id);
    }
}
