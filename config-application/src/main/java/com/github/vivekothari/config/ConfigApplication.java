package com.github.vivekothari.config;

import com.bendb.dropwizard.redis.JedisBundle;
import com.bendb.dropwizard.redis.JedisFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vivekkothari.trim.ConfigTrimmingBundle;
import com.github.vivekothari.config.config.ConfigAppConfig;
import com.github.vivekothari.config.dao.ESSchemaDao;
import com.github.vivekothari.config.dao.IConfigDao;
import com.github.vivekothari.config.dao.ISchemaDao;
import com.github.vivekothari.config.dao.RedisConfigDao;
import com.github.vivekothari.config.health.ESHealthCheck;
import com.github.vivekothari.config.managed.ManagedESConnection;
import com.github.vivekothari.config.resource.ConfigResource;
import com.github.vivekothari.config.resource.SchemaResource;
import com.github.vivekothari.config.service.ConfigServiceImpl;
import com.github.vivekothari.config.service.IConfigService;
import com.github.vivekothari.config.service.ISchemaService;
import com.github.vivekothari.config.service.SchemaServiceImpl;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import redis.clients.jedis.JedisPool;

/**
 * @author vivek.kothari on 17/07/16.
 */
public class ConfigApplication
        extends Application<ConfigAppConfig> {

    private final SwaggerBundle<ConfigAppConfig> swaggerBundle = new SwaggerBundle<ConfigAppConfig>() {

        @Override
        protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(ConfigAppConfig configuration) {
            return configuration.getSwagger();
        }
    };

    public static void main(String[] args) throws Exception {
        new ConfigApplication().run(args);
    }

    @Override
    public void initialize(final Bootstrap<ConfigAppConfig> bootstrap) {
        super.initialize(bootstrap);
        bootstrap.setConfigurationSourceProvider(new SubstitutingSourceProvider(bootstrap.getConfigurationSourceProvider(), new EnvironmentVariableSubstitutor()));
        bootstrap.addBundle(new ConfigTrimmingBundle());
        bootstrap.addBundle(swaggerBundle);
        bootstrap.addBundle(new JedisBundle<ConfigAppConfig>() {
            @Override
            public JedisFactory getJedisFactory(ConfigAppConfig configuration) {
                return configuration.getJedisFactory();
            }
        });
    }

    public void run(final ConfigAppConfig configuration, final Environment environment) throws Exception {
        final JedisPool jedisPool = configuration.getJedisFactory()
                                                 .build(environment);
        ManagedESConnection managedESConnection = new ManagedESConnection(configuration.getEsConfig());
        final ObjectMapper objectMapper = environment.getObjectMapper();

        final ISchemaDao schemaDao = new ESSchemaDao(objectMapper, configuration.getEsConfig(), managedESConnection);
        final ISchemaService schemaService = new SchemaServiceImpl(schemaDao);
        final SchemaResource schemaResource = new SchemaResource(schemaService);

        final IConfigDao configDao = new RedisConfigDao(jedisPool, objectMapper);
        final IConfigService configService = new ConfigServiceImpl(configDao, schemaService);
        final ConfigResource configResource = new ConfigResource(configService);

        environment.healthChecks()
                   .register("elastic-search", new ESHealthCheck(managedESConnection, configuration.getEsConfig()));

        environment.jersey()
                   .register(schemaResource);
        environment.jersey()
                   .register(configResource);
        environment.lifecycle()
                   .manage(managedESConnection);

    }
}
