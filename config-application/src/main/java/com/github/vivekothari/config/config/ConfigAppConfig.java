package com.github.vivekothari.config.config;

import com.bendb.dropwizard.redis.JedisFactory;
import io.dropwizard.Configuration;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author vivek.kothari on 17/07/16.
 */
@Getter
public class ConfigAppConfig extends Configuration {

    @Valid
    private SwaggerBundleConfiguration swagger = new SwaggerBundleConfiguration();

    private String indexPagePath = "service/index.html";

    @Valid
    private JedisFactory jedisFactory = new JedisFactory();

    @Valid
    @NotNull
    private ESConfig esConfig;

}
