package com.github.vivekothari.config.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;
import com.github.vivekothari.config.api.Metadata;
import com.github.vivekothari.config.api.Schema;
import com.github.vivekothari.config.bean.MetaResponse;
import com.github.vivekothari.config.bean.PagingParam;
import com.github.vivekothari.config.bean.SortBy;
import com.github.vivekothari.config.config.ESConfig;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.max.Max;
import org.elasticsearch.search.aggregations.metrics.max.MaxBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author vivek.kothari on 17/07/16.
 */
@Slf4j
public class ESSchemaDao
        implements ISchemaDao {
    private static final String METADATA = "metadata";
    private static final String CONFIG_NAME = "configName";
    private static final String CONFIG_VERSION = "version";
    private static final String MAX_VERSION = "maxVersion";
    private static final int RECORDS_LIMIT_PER_REQUEST = 100;
    private static final String CREATED_AT = "createdAt";
    private static final SortOrder DEFAULT_SORT_ORDER = SortOrder.DESC;
    private static final String LATEST = "latest";

    private static final String TENANT = "tenant";

    private final ObjectMapper mapper;
    private final ESConfig esConfig;
    private final Supplier<Client> clientSupplier;

    public ESSchemaDao(final ObjectMapper mapper, final ESConfig esConfig, final Supplier<Client> clientSupplier) {
        this.mapper = mapper;
        this.esConfig = esConfig;
        this.clientSupplier = clientSupplier;
    }

    /**
     * Return a list of distinct tenants
     *
     * @return all tenants
     */
    @Override
    public List<String> fetchAllTenants() {
        final SearchResponse response = clientSupplier.get()
                                                      .prepareSearch(esConfig.getIndexName())
                                                      .setTypes(esConfig.getIndexType())
                                                      .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                                                      .setQuery(QueryBuilders.matchAllQuery())
                                                      .addAggregation(AggregationBuilders.terms(TENANT)
                                                                                         .field("metadata.tenant")
                                                                                         .size(0))
                                                      .setExplain(false)
                                                      .get();

        final Terms terms = response.getAggregations()
                                    .get(TENANT);
        final List<Terms.Bucket> buckets = terms.getBuckets();
        return buckets.stream()
                      .map(MultiBucketsAggregation.Bucket::getKey)
                      .map(String::valueOf)
                      .collect(Collectors.toList());
    }


    /**
     * Persists the schema
     *
     * @param schema
     *         to save
     *
     * @return created schema
     */
    @Override
    public Schema persist(final Schema schema) {
        try {
            String oldId = null;
            final Metadata metadata = schema.getMetadata();
            final String configName = metadata.getConfigName();
            final String tenant = metadata.getTenant();
            final Schema latestVersion = getLatestSchema(tenant, configName);

            // Version changes for same schema entry
            int version = 1;
            if (latestVersion != null) {
                final Metadata latestVersionMetadata = latestVersion.getMetadata();
                final int lastVersion = latestVersionMetadata.getVersion();
                oldId = latestVersionMetadata.getId();

                // Setting null to the fields which are not used for json
                // comparison
                latestVersionMetadata.setId(null);
                latestVersionMetadata.setVersion(null);
                latestVersionMetadata.setCreatedAt(null);
                latestVersionMetadata.setLatest(false);

                metadata.setId(null);
                metadata.setVersion(null);
                metadata.setCreatedAt(null);
                metadata.setLatest(false);

                if (jsonDiff(latestVersion, schema)) {
                    version = lastVersion + 1;
                    log.info("New schema json found, hence incrementing the version to - {}", version);
                } else { // Same schema is coming without any change
                    log.warn("Schema Json is same as before...");
                    return null;
                }
            }

            final String id = getId(tenant, configName, version);
            metadata.setVersion(version);
            metadata.setId(id);
            metadata.setCreatedAt(getCurrTS());
            metadata.setLatest(true);

            saveSchema(id, schema);
            if (oldId != null) setLatestFalse(oldId);

        } catch (final Exception e) {
            log.error("Error - {}", e.getMessage(), e);
            throw new RuntimeException(e);
        }
        log.debug("Successfully persisted the schema: {}", schema);
        return schema;
    }

    /**
     * Returns the actual schema + meta for this version of the schema for the
     * given tenant
     *
     * @param name
     *         to fetch
     * @param version
     *         to fetch
     *
     * @return schema
     *
     * @throws IOException
     */
    @Override
    public Schema fetchSchema(final String tenant, final String name, final int version) throws IOException {
        log.debug("Get start Time: {}", System.currentTimeMillis());
        final GetResponse response = clientSupplier.get()
                                                   .prepareGet(esConfig.getIndexName(), esConfig.getIndexType(), getId(tenant, name, version))
                                                   .get();
        log.debug("Get stop Time: {}", System.currentTimeMillis());
        return Strings.isNullOrEmpty(response.getSourceAsString()) ? null : mapper.readValue(response.getSourceAsString(), Schema.class);
    }

    @Override
    public Schema getLatestSchema(final String tenant, final String configName) throws IOException {
        final TermQueryBuilder builder = QueryBuilders.termQuery(METADATA + '.' + CONFIG_NAME, configName);
        final MaxBuilder maxBuilder = AggregationBuilders.max(MAX_VERSION)
                                                         .field(METADATA + '.' + CONFIG_VERSION);

        SearchRequestBuilder query = clientSupplier.get()
                                                   .prepareSearch(esConfig.getIndexName())
                                                   .setTypes(esConfig.getIndexType())
                                                   .setIndicesOptions(IndicesOptions.lenientExpandOpen())
                                                   .setSearchType(SearchType.QUERY_AND_FETCH)
                                                   .setQuery(QueryBuilders.constantScoreQuery(builder))
                                                   .setSize(0)
                                                   .addAggregation(maxBuilder);

        if (log.isDebugEnabled()) {
            log.debug("Max query: {}", query.internalBuilder());
        }
        final SearchResponse response = query.get();

        final Optional<Aggregations> aggregations = Optional.ofNullable(response.getAggregations());
        if (aggregations.isPresent()) {
            final Max aggregation = aggregations.get()
                                                .get(MAX_VERSION);
            return fetchSchema(tenant, configName, (int) aggregation.getValue());
        }
        return null;
    }

    /**
     * Return a list of latest metas for given tenant
     *
     * @param tenant
     *         to fetch
     * @param fromParam
     *         to fetch
     * @param sizeParam
     *         to fetch
     * @param sortBy
     *         to fetch
     *
     * @return meta data
     *
     * @throws IOException
     */
    @Override
    public MetaResponse fetchMeta(final String tenant, final Integer fromParam, final Integer sizeParam, final SortBy sortBy) throws IOException {
        final int from = fromParam == null ? 0 : fromParam;
        final int size = sizeParam == null ? RECORDS_LIMIT_PER_REQUEST : sizeParam;

        final BoolQueryBuilder query = QueryBuilders.boolQuery()
                                                    .must(QueryBuilders.termQuery(String.join(".", METADATA, LATEST), true))
                                                    .must(QueryBuilders.termQuery(String.join(".", METADATA, TENANT), tenant));

        final SearchRequestBuilder requestBuilder = clientSupplier.get()
                                                                  .prepareSearch(esConfig.getIndexName())
                                                                  .setTypes(esConfig.getIndexType())
                                                                  .addSort(getSortBuilder(sortBy))
                                                                  .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                                                                  .setQuery(query)
                                                                  .setSize(size)
                                                                  .setFrom(from)
                                                                  .setExplain(false);
        log.info(requestBuilder.toString());
        final SearchResponse response = requestBuilder.get();

        return getResponse(response, from, size);
    }

    /**
     * Return a list of metas for all versions for this given schema for the
     * given tenant
     *
     * @param tenant
     *         to fetch
     * @param configName
     *         to fetch
     * @param fromParam
     *         to fetch
     * @param sizeParam
     *         to fetch
     * @param sortBy
     *         to fetch
     *
     * @return schema meta
     *
     * @throws IOException
     */
    @Override
    public MetaResponse fetchMeta(final String tenant, final String configName, final Integer fromParam, final Integer sizeParam, final SortBy sortBy) throws IOException {

        final int from = fromParam == null ? 0 : fromParam;
        final int size = sizeParam == null ? RECORDS_LIMIT_PER_REQUEST : sizeParam;

        final BoolQueryBuilder query = QueryBuilders.boolQuery()
                                                    .must(QueryBuilders.termQuery(String.join(".", METADATA, CONFIG_NAME), configName))
                                                    .must(QueryBuilders.termQuery(String.join(".", METADATA, TENANT), tenant));

        final SearchResponse response = clientSupplier.get()
                                                      .prepareSearch(esConfig.getIndexName())
                                                      .setTypes(esConfig.getIndexType())
                                                      .addSort(getSortBuilder(sortBy))
                                                      .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                                                      .setQuery(query)
                                                      .setSize(size)
                                                      .setFrom(from)
                                                      .setExplain(false)
                                                      .execute()
                                                      .actionGet();

        return getResponse(response, from, size);
    }

    /**
     * To get the diff between two json. Returns true if there is difference
     */
    private boolean jsonDiff(final Schema lastSchema, final Schema newSchema) {
        try {
            final JsonNode first = mapper.readTree(mapper.writeValueAsString(lastSchema));
            final JsonNode second = mapper.readTree(mapper.writeValueAsString(newSchema));
            return JsonDiff.asJson(first, second)
                           .size() > 0;
        } catch (final IOException e) {
            log.error("Unable to process JSON - {}", e.getMessage(), e);
            return false;
        }
    }

    /**
     * Generate schema id from given JSON schema
     */
    private String getId(final String tenant, final String configName, final int version) {
        return String.format("%s:%s:%s", tenant, configName, version);
    }

    private Timestamp getCurrTS() {
        return new Timestamp(System.currentTimeMillis());
    }

    /**
     * Save schema
     */
    private void saveSchema(String id, Schema schema) throws ElasticsearchException, JsonProcessingException {
        clientSupplier.get()
                      .prepareIndex(esConfig.getIndexName(), esConfig.getIndexType(), id)
                      .setCreate(true)
//                      .setConsistencyLevel(WriteConsistencyLevel.ALL)
                      .setSource(mapper.writeValueAsString(schema))
                      .setRefresh(true)
                      .get();
    }

    /**
     * To update the latest status
     */
    private void setLatestFalse(String id) throws ElasticsearchException {
        clientSupplier.get()
                      .prepareUpdate(esConfig.getIndexName(), esConfig.getIndexType(), id)
                      .setDoc("{\"metadata\" : {\"latest\" : false}}".getBytes())
//                      .setConsistencyLevel(WriteConsistencyLevel.ALL)
                      .get();
    }

    private SortBuilder getSortBuilder(SortBy sortBy) {
        final FieldSortBuilder sorting = new FieldSortBuilder(CREATED_AT).order(sortBy == null ? DEFAULT_SORT_ORDER : getSortOrder(sortBy))
                                                                         .ignoreUnmapped(true);
        return sorting;
    }

    private SortOrder getSortOrder(final SortBy sortBy) {
        return sortBy.equals(SortBy.asc) ? SortOrder.ASC : SortOrder.DESC;
    }

    private MetaResponse getResponse(final SearchResponse response, final int from, final int size) throws IOException {
        final List<Metadata> metaDatas = getSchemaMetaDataFromHits(response);
        final PagingParam pagingParam = new PagingParam(from, size, response.getHits()
                                                                            .totalHits());
        return new MetaResponse(metaDatas, pagingParam);
    }

    private List<Metadata> getSchemaMetaDataFromHits(final SearchResponse response) throws IOException {
        final List<Metadata> metaDatas = Lists.newArrayList();
        if (response != null) {
            Schema schema;
            for (final SearchHit hit : response.getHits()) {
                schema = mapper.readValue(hit.getSourceAsString(), Schema.class);
                metaDatas.add(schema.getMetadata());
            }
        }
        return metaDatas;
    }
}
