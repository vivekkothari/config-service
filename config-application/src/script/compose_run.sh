#!/usr/bin/env bash

./local_es_setup.sh
java -jar -XX:+${GC_ALGO} server.jar server ${CONFIG_ENV}-config.yml
