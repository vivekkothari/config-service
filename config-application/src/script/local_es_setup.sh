#!/usr/bin/env bash

index_exists=`curl -I 'http://localhost:9200/config_schema' | grep '404' | wc -l`

if [ $index_exists == 1 ]
then
  curl -X PUT "http://localhost:9200/config_schema"
fi

curl -X PUT -H "Content-Type: application/json" -d '{
    "properties": {
        "schema": {
            "type": "object",
            "enabled": false
        },
        "metadata": {
            "properties": {
                "id": {
                    "type": "string"
                },
                "createdAt": {
                    "type": "long"
                },
                "latest": {
                    "type": "boolean"
                },
                "configName": {
                    "type": "string",
                    "index": "not_analyzed",
                    "doc_values": true,
                    "copy_to": [
                        "search"
                    ]
                },
                "tenant": {
                    "type": "string",
                    "index": "not_analyzed",
                    "doc_values": true,
                    "copy_to": [
                        "search"
                    ]
                },
                "version": {
                    "type": "long"
                },
                "search": {
                    "type": "string"
                },
                "configType": {
                    "type": "string"
                }
            }
        },
        "search": {
            "type": "string"
        }
    }
}' "http://localhost:9200/config_schema/_mapping/config_schema"
