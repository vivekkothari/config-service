package com.github.vivekothari.config.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vivek.kothari on 28/03/16.
 */
@Data
@AllArgsConstructor
public class ValidationResponse {
    private static final ObjectMapper mapper = new ObjectMapper();

    private ValidationStatus status;
    private List<String> messages = new ArrayList<>();

    @Override
    public String toString() {
        try {
            return mapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
