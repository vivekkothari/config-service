package com.github.vivekothari.config.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

/**
 * @author vivek.kothari on 22/03/16.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Metadata {

    private String id;

    @NotNull
    private String configName;

    @NotNull
    private String tenant;

    @NotNull
    private ConfigType configType;

    private Integer version;

    private Timestamp createdAt;

    private boolean latest;

    public enum ConfigType {
        standard, keyvalue;
    }

}
