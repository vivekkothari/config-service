package com.github.vivekothari.config.api;

/**
 * @author vivek.kothari on 28/03/16.
 */
public enum ValidationStatus {
    SUCCESS, VALIDATION_FAILED, IO_EXCEPTION, JSON_PROCESSING_EXCEPTION, OTHER_EXCEPTION
}
