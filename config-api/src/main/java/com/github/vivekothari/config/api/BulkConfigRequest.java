package com.github.vivekothari.config.api;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author vivek.kothari on 28/03/16.
 */
@Data
@AllArgsConstructor
public class BulkConfigRequest {

    @NotNull
    private String id;

    @NotNull
    private JsonNode data;

}
