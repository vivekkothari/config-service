package com.github.vivekothari.config.api;

import com.fasterxml.jackson.databind.JsonNode;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @author vivek.kothari on 22/03/16.
 */
@Getter
@Setter
@AllArgsConstructor
public class Schema {

    @NotNull
    @Valid
    @ApiModelProperty(required = true)
    private Metadata metadata;

    @NotNull
    @ApiModelProperty(value = "Json schema (refer jsonschema.net)", required = true)
    private JsonNode schema;
}
