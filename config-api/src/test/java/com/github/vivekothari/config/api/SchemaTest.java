package com.github.vivekothari.config.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author vivek.kothari on 18/07/16.
 */
public class SchemaTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void getMetadata() throws Exception {
        final Metadata metadata = Metadata.builder()
                                          .build();
        final JsonNode jsonNode = mapper.readTree("{}");
        Schema schema = new Schema(metadata, jsonNode);
        Assert.assertEquals(metadata, schema.getMetadata());
    }

    @Test
    public void getSchema() throws Exception {
        final Metadata metadata = Metadata.builder()
                                          .build();
        final JsonNode jsonNode = mapper.readTree("{}");
        Schema schema = new Schema(metadata, jsonNode);
        Assert.assertEquals(jsonNode, schema.getSchema());
    }

}