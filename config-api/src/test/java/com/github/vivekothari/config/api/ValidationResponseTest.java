package com.github.vivekothari.config.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * @author vivek.kothari on 18/07/16.
 */
public class ValidationResponseTest {

    @Test
    public void testToString() throws Exception {
        ValidationResponse response = new ValidationResponse(ValidationStatus.IO_EXCEPTION, Arrays.asList("message"));
        final String s = response.toString();
        ObjectMapper mapper = new ObjectMapper();
        Assert.assertEquals(mapper.writeValueAsString(response), s);
    }

    @Test
    public void getStatus() throws Exception {
        ValidationResponse response = new ValidationResponse(ValidationStatus.IO_EXCEPTION, Arrays.asList("message"));
        Assert.assertEquals(ValidationStatus.IO_EXCEPTION, response.getStatus());
    }

    @Test
    public void getMessages() throws Exception {
        final List<String> messages = Arrays.asList("message");
        ValidationResponse response = new ValidationResponse(ValidationStatus.IO_EXCEPTION, messages);
        Assert.assertEquals(messages, response.getMessages());
    }

}