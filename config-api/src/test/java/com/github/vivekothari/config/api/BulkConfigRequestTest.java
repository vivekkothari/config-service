package com.github.vivekothari.config.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author vivek.kothari on 18/07/16.
 */
public class BulkConfigRequestTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Test
    public void getId() throws Exception {
        final JsonNode jsonNode = mapper.readTree("{}");
        BulkConfigRequest request = new BulkConfigRequest("id", jsonNode);
        Assert.assertEquals("id", request.getId());
    }

    @Test
    public void getData() throws Exception {
        final JsonNode jsonNode = mapper.readTree("{}");
        BulkConfigRequest request = new BulkConfigRequest("id", jsonNode);
        Assert.assertEquals(jsonNode, request.getData());
    }

}