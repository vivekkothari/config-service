package com.github.vivekothari.config.api;

import org.junit.Test;

import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;

/**
 * @author vivek.kothari on 28/07/16.
 */
public class MetadataTest {

    @Test
    public void test() {
        final Timestamp createdAt = new Timestamp(System.currentTimeMillis());
        final Metadata metadata = Metadata.builder()
                                          .tenant("tenant")
                                          .configName("config")
                                          .version(1)
                                          .configType(Metadata.ConfigType.standard)
                                          .createdAt(createdAt)
                                          .id("id")
                                          .latest(true)
                                          .build();
        assertEquals("tenant", metadata.getTenant());
        assertEquals("config", metadata.getConfigName());
        assertEquals("id", metadata.getId());
        assertEquals(Integer.valueOf(1), metadata.getVersion());
        assertEquals(createdAt, metadata.getCreatedAt());
        assertEquals(true, metadata.isLatest());
        assertEquals(Metadata.ConfigType.standard, metadata.getConfigType());
    }

}