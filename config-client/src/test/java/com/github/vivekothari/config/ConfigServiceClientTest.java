package com.github.vivekothari.config;

import org.apache.http.impl.client.CloseableHttpClient;
import org.junit.Assert;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 * @author vivek.kothari on 17/07/16.
 */
public class ConfigServiceClientTest {

    @Test
    public void init() throws Exception {
        final ConfigServiceConfig config = new ConfigServiceConfig();
        config.setBaseUrl("http://localhost:8080");

        final ConfigServiceClient configServiceClient = new ConfigServiceClient(config);
        configServiceClient.init();
        Assert.assertNotNull(configServiceClient.getConfigService());
    }

    @Test
    public void isStarted() throws Exception {
        final ConfigServiceConfig config = new ConfigServiceConfig();
        config.setBaseUrl("http://localhost:8080");

        final ConfigServiceClient configServiceClient = new ConfigServiceClient(config);
        configServiceClient.init();
        Assert.assertEquals(true, configServiceClient.started.get());
    }

    @Test(expected = IllegalStateException.class)
    public void destroy() throws Exception {
        final ConfigServiceConfig config = new ConfigServiceConfig();
        config.setBaseUrl("http://localhost:8080");

        final ConfigServiceClient configServiceClient = new ConfigServiceClient(config);
        configServiceClient.init();
        configServiceClient.destroy();
        configServiceClient.getConfigService();
    }

    @Test
    public void testHttpClient() throws Exception {
        final ConfigServiceConfig config = new ConfigServiceConfig();
        config.setBaseUrl("http://localhost:8080");
        config.setConnectTimeoutMillis(0);
        config.setReadTimeoutMillis(0);

        final CloseableHttpClient httpClient = mock(CloseableHttpClient.class);
        final ConfigServiceClient configServiceClient = new ConfigServiceClient(config, httpClient);
        configServiceClient.init();
        configServiceClient.destroy();
        verify(httpClient, times(1)).close();
    }

}