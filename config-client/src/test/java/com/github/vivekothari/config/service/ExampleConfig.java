package com.github.vivekothari.config.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author vivek.kothari on 17/07/16.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExampleConfig {

    private String name;

    private String profession;

}
