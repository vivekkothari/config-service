package com.github.vivekothari.config.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vivekothari.config.ConfigServiceConfig;
import com.github.vivekothari.config.api.BulkConfigRequest;
import com.github.vivekothari.config.feign.ConfigClient;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author vivek.kothari on 18/07/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class ConfigServiceTest {

    @Mock
    private ConfigClient configClient;

    private ConfigService configService;

    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setUp() throws Exception {
        ConfigServiceConfig configServiceConfig = new ConfigServiceConfig();
        configServiceConfig.setBaseUrl("http://localhost:8080");
        configService = new ConfigService(configClient, configServiceConfig, mapper);
    }

    @Test
    public void createStandard() throws Exception {
        final ExampleConfig exampleConfig = new ExampleConfig("Vivek", "SDE");
        configService.createStandard("tenant", "config", 1, exampleConfig);
        verify(configClient, times(1)).createStandard("tenant", "config", 1, mapper.convertValue(exampleConfig, JsonNode.class));
    }

    @Test
    public void getStandardConfig() throws Exception {
        when(configClient.getStandardConfig("tenant", "config", 1)).thenReturn(null);
        final Optional<JsonNode> config = configService.getStandardConfig("tenant", "config", 1);
        Assert.assertEquals(false, config.isPresent());
        verify(configClient, times(1)).getStandardConfig("tenant", "config", 1);

        Mockito.reset(configClient);

        final ExampleConfig exampleConfig = new ExampleConfig("Vivek", "SDE");

        final JsonNode value = mapper.convertValue(exampleConfig, JsonNode.class);
        when(configClient.getStandardConfig("tenant", "config", 1)).thenReturn(value);
        final Optional<JsonNode> standardConfig = configService.getStandardConfig("tenant", "config", 1);
        Assert.assertEquals(true, standardConfig.isPresent());
        verify(configClient, times(1)).getStandardConfig("tenant", "config", 1);
        final ExampleConfig exampleConfig1 = standardConfig.map(c -> mapper.convertValue(c, ExampleConfig.class))
                                                           .orElseThrow(() -> new AssertionError());
        Assert.assertEquals("Vivek", exampleConfig1.getName());
    }

    @Test
    public void getStandardConfig_object() throws Exception {
        final ExampleConfig exampleConfig = new ExampleConfig("Vivek", "SDE");
        final JsonNode value = mapper.convertValue(exampleConfig, JsonNode.class);
        when(configClient.getStandardConfig("tenant", "config", 1)).thenReturn(value);
        final Optional<ExampleConfig> standardConfig = configService.getStandardConfig("tenant", "config", 1, ExampleConfig.class);
        Assert.assertEquals(true, standardConfig.isPresent());
        verify(configClient, times(1)).getStandardConfig("tenant", "config", 1);
        Assert.assertEquals("Vivek", standardConfig.get()
                                                   .getName());
    }

    @Test
    public void getStandardConfig_object_cache() throws Exception {
        final ExampleConfig exampleConfig = new ExampleConfig("Vivek", "SDE");
        final JsonNode value = mapper.convertValue(exampleConfig, JsonNode.class);
        when(configClient.getStandardConfig("tenant", "config", 1)).thenReturn(value);
        final Optional<ExampleConfig> firstCall = configService.getStandardConfig("tenant", "config", 1, ExampleConfig.class);
        final Optional<ExampleConfig> secondCall = configService.getStandardConfig("tenant", "config", 1, ExampleConfig.class);
        final Optional<ExampleConfig> thirdCall = configService.getStandardConfig("tenant", "config", 1, ExampleConfig.class);
        verify(configClient, times(1)).getStandardConfig("tenant", "config", 1);
    }

    @Test
    public void createKVConfig() throws Exception {
        final ExampleConfig exampleConfig = new ExampleConfig("Vivek", "SDE");
        configService.createKVConfig("tenant", "config", 1, "id", exampleConfig);
        verify(configClient, times(1)).createKVConfig("tenant", "config", 1, "id", mapper.convertValue(exampleConfig, JsonNode.class));
    }

    @Test
    public void createBulkKVConfig() throws Exception {
        final ExampleConfig config1 = new ExampleConfig("Vivek", "SDE");
        final ExampleConfig config2 = new ExampleConfig("Umesh", "SDE");

        final ImmutableMap<String, ExampleConfig> map = ImmutableMap.of("id1", config1, "id2", config2);
        configService.createBulkKVConfig("tenant", "config", 1, map);
        final List<BulkConfigRequest> list = Lists.newArrayList(new BulkConfigRequest("id1", mapper.convertValue(config1, JsonNode.class)),
                                                                new BulkConfigRequest("id2", mapper.convertValue(config2, JsonNode.class)));
        verify(configClient).createBulkKVConfig("tenant", "config", 1, list);
    }

    @Test
    public void getKVConfig() throws Exception {
        when(configClient.getKVConfig("tenant", "config", 1, "id")).thenReturn(null);
        final Optional<JsonNode> config = configService.getKVConfig("tenant", "config", 1, "id");
        Assert.assertEquals(false, config.isPresent());
        verify(configClient, times(1)).getKVConfig("tenant", "config", 1, "id");

        Mockito.reset(configClient);

        final ExampleConfig exampleConfig = new ExampleConfig("Vivek", "SDE");

        final JsonNode value = mapper.convertValue(exampleConfig, JsonNode.class);
        when(configClient.getKVConfig("tenant", "config", 1, "id")).thenReturn(value);
        final Optional<JsonNode> standardConfig = configService.getKVConfig("tenant", "config", 1, "id");
        Assert.assertEquals(true, standardConfig.isPresent());
        verify(configClient, times(1)).getKVConfig("tenant", "config", 1, "id");
        final ExampleConfig exampleConfig1 = standardConfig.map(c -> mapper.convertValue(c, ExampleConfig.class))
                                                           .orElseThrow(() -> new AssertionError());
        Assert.assertEquals("Vivek", exampleConfig1.getName());
    }

    @Test
    public void getKVConfig_object() throws Exception {
        final ExampleConfig exampleConfig = new ExampleConfig("Vivek", "SDE");
        final JsonNode value = mapper.convertValue(exampleConfig, JsonNode.class);
        when(configClient.getKVConfig("tenant", "config", 1, "id")).thenReturn(value);
        final Optional<ExampleConfig> standardConfig = configService.getKVConfig("tenant", "config", 1, "id", ExampleConfig.class);
        Assert.assertEquals(true, standardConfig.isPresent());
        verify(configClient, times(1)).getKVConfig("tenant", "config", 1, "id");
        Assert.assertEquals("Vivek", standardConfig.get()
                                                   .getName());
    }

    @Test
    public void getKVConfig_object_cache() throws Exception {
        final ExampleConfig exampleConfig = new ExampleConfig("Vivek", "SDE");
        final JsonNode value = mapper.convertValue(exampleConfig, JsonNode.class);
        when(configClient.getKVConfig("tenant", "config", 1, "id")).thenReturn(value);
        final Optional<ExampleConfig> first = configService.getKVConfig("tenant", "config", 1, "id", ExampleConfig.class);
        final Optional<ExampleConfig> second = configService.getKVConfig("tenant", "config", 1, "id", ExampleConfig.class);
        final Optional<ExampleConfig> third = configService.getKVConfig("tenant", "config", 1, "id", ExampleConfig.class);
        verify(configClient, times(1)).getKVConfig("tenant", "config", 1, "id");

    }

}