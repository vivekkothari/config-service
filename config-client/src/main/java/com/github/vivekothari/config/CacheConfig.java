package com.github.vivekothari.config;

import lombok.Data;

/**
 * @author vivek.kothari on 25/01/16.
 */
@Data
public class CacheConfig {

    private int initialCapacity = 100;
    private long expireAfterAccessInSeconds = 10;
    private long maximumSize = 10000;
}
