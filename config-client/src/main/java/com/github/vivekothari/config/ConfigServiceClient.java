package com.github.vivekothari.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vivekothari.config.feign.ConfigClient;
import com.github.vivekothari.config.service.ConfigService;
import feign.Feign;
import feign.Logger;
import feign.Request;
import feign.httpclient.ApacheHttpClient;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.jaxrs.JAXRSContract;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author vivek.kothari on 25/01/16.
 */
@Slf4j
public class ConfigServiceClient {

    final AtomicBoolean started = new AtomicBoolean(false);
    private final ConfigServiceConfig config;
    private final CloseableHttpClient httpClient;
    private ConfigService configService;

    public ConfigServiceClient(final ConfigServiceConfig config) {
        this(config, HttpClients.createDefault());
    }

    public ConfigServiceClient(final ConfigServiceConfig config, final CloseableHttpClient httpClient) {
        this.config = config;
        this.httpClient = httpClient;
    }

    public void init() throws Exception {
        log.info("Starting config client with config - {}", config);

        final ObjectMapper mapper = new ObjectMapper();

        final ConfigClient configClient = builder(mapper).target(ConfigClient.class, config.getBaseUrl());

        this.configService = new ConfigService(configClient, config, mapper);

        log.info("Client started..");
        this.started.set(true);
    }

    private Feign.Builder builder(final ObjectMapper mapper) {
        return Feign.builder()
                    .client(new ApacheHttpClient(httpClient))
                    .contract(new JAXRSContract())
                    .logLevel((null != config.getLogLevel()) ? this.config.getLogLevel() : Logger.Level.BASIC)
                    .options(new Request.Options((0 != config.getConnectTimeoutMillis()) ? this.config.getConnectTimeoutMillis() : 10,
                                                 (0 != config.getReadTimeoutMillis()) ? this.config.getReadTimeoutMillis() : 60))
                    .encoder(new JacksonEncoder(mapper))
                    .decoder(new JacksonDecoder(mapper))
                    .errorDecoder(config.getErrorDecoder());
    }

    public void destroy() throws Exception {
        isStarted();
        log.info("Client stopped..");
        httpClient.close();
        this.started.set(false);
    }

    private void isStarted() {
        if (!started.get()) {
            log.error("Config client is not yet started...");
            throw new IllegalStateException("Client not initialized yet.");
        }
    }

    public ConfigService getConfigService() {
        isStarted();
        return this.configService;
    }

}
