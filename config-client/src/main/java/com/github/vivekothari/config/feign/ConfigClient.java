package com.github.vivekothari.config.feign;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.vivekothari.config.api.BulkConfigRequest;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import java.util.List;
import java.util.Map;

/**
 * @author vivek.kothari on 29/03/16.
 */
public interface ConfigClient {

    @PUT
    @Path("/v1/config/{tenant}/{configName}/{version}")
    @Consumes("application/json")
    JsonNode createStandard(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                            @NotNull @PathParam("version") final int version, @NotNull @Valid final JsonNode jsonNode);


    @GET
    @Path("/v1/config/{tenant}/{configName}/{version}")
    JsonNode getStandardConfig(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                               @NotNull @PathParam("version") final int version);

    @PUT
    @Path("/v1/config/{tenant}/{configName}/{version}/{id}")
    @Consumes("application/json")
    Map<String, JsonNode> createKVConfig(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                                         @NotNull @PathParam("version") final int version, @NotNull @PathParam("id") final String id, @NotNull @Valid final JsonNode jsonNode);

    @PUT
    @Path("/v1/config/{tenant}/{configName}/{version}/bulk")
    Map<String, JsonNode> createBulkKVConfig(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                                             @NotNull @PathParam("version") final int version, @Valid final List<BulkConfigRequest> bulkConfigRequests);

    @GET
    @Path("/v1/config/{tenant}/{configName}/{version}/{id}")
    JsonNode getKVConfig(@NotNull @PathParam("tenant") final String tenant, @NotNull @PathParam("configName") final String configName,
                         @NotNull @PathParam("version") final int version, @NotNull @PathParam("id") final String id);


}
