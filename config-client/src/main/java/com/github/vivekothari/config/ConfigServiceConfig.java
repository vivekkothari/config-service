package com.github.vivekothari.config;

import feign.Logger;
import feign.codec.ErrorDecoder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author vivek.kothari on 25/01/16.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConfigServiceConfig {
    private int connectTimeoutMillis = 10;
    private int readTimeoutMillis = 60;
    private Logger.Level logLevel = Logger.Level.BASIC;
    private CacheConfig cacheConfig = new CacheConfig();
    private String baseUrl;
    private ErrorDecoder errorDecoder = (methodKey, response) -> new RuntimeException(
            String.format("Error executing %s. Server returned response \n%s", methodKey, response.toString()));
}
