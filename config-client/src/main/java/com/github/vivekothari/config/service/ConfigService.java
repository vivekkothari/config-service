package com.github.vivekothari.config.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.vivekothari.config.CacheConfig;
import com.github.vivekothari.config.ConfigServiceConfig;
import com.github.vivekothari.config.api.BulkConfigRequest;
import com.github.vivekothari.config.feign.ConfigClient;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * @author vivek.kothari on 17/07/16.
 */
@Slf4j
public class ConfigService {

    private final ConfigClient configClient;
    private final ConfigServiceConfig config;
    private final ObjectMapper objectMapper;
    private LoadingCache<StandardConfigKey, JsonNode> standardConfigCache;
    private LoadingCache<KVConfigKey, JsonNode> kvConfigCache;

    public ConfigService(final ConfigClient configClient, final ConfigServiceConfig config, final ObjectMapper objectMapper) {
        this.configClient = configClient;
        this.config = config;
        this.objectMapper = objectMapper;
        this.initCache();
    }

    @VisibleForTesting
    void initCache() {
        CacheLoader<StandardConfigKey, JsonNode> cacheLoader = new CacheLoader<StandardConfigKey, JsonNode>() {
            @Override
            public JsonNode load(final StandardConfigKey key) throws Exception {
                return configClient.getStandardConfig(key.tenant, key.configName, key.version);
            }
        };
        Supplier<CacheBuilder<Object, Object>> supplier = () -> {
            final CacheConfig cacheConfig = config.getCacheConfig();
            return CacheBuilder.newBuilder()
                               .initialCapacity(cacheConfig.getInitialCapacity())
                               .expireAfterAccess(cacheConfig.getExpireAfterAccessInSeconds(), TimeUnit.SECONDS)
                               .maximumSize(cacheConfig.getMaximumSize());
        };

        this.standardConfigCache = supplier.get()
                                           .build(cacheLoader);


        CacheLoader<KVConfigKey, JsonNode> kvCacheLoader = new CacheLoader<KVConfigKey, JsonNode>() {
            @Override
            public JsonNode load(final KVConfigKey key) throws Exception {
                return configClient.getKVConfig(key.tenant, key.configName, key.version, key.id);
            }
        };
        this.kvConfigCache = supplier.get()
                                     .build(kvCacheLoader);
    }

    public <T> JsonNode createStandard(final String tenant, final String configName, final int version, final T object) {
        return createStandard(tenant, configName, version, objectMapper.convertValue(object, JsonNode.class));
    }

    public JsonNode createStandard(final String tenant, final String configName, final int version, final JsonNode jsonNode) {
        return configClient.createStandard(tenant, configName, version, jsonNode);
    }

    public <T> Optional<T> getStandardConfig(final String tenant, final String configName, final int version, final Class<T> clazz) {
        try {
            final Optional<JsonNode> jsonNode = getStandardConfig(tenant, configName, version);
            return jsonNode.map(node -> objectMapper.convertValue(node, clazz));
        } catch (Exception e) {
            log.error("Error", e);
            return Optional.empty();
        }
    }

    public Optional<JsonNode> getStandardConfig(final String tenant, final String configName, final int version) {
        try {
            return Optional.of(standardConfigCache.get(new StandardConfigKey(tenant, configName, version)));
        } catch (Exception e) {
            log.error("Error", e);
            return Optional.empty();
        }
    }

    public <T> Map<String, JsonNode> createKVConfig(final String tenant, final String configName, final int version, final String id, final T object) {
        return createKVConfig(tenant, configName, version, id, objectMapper.convertValue(object, JsonNode.class));
    }

    public Map<String, JsonNode> createKVConfig(final String tenant, final String configName, final int version, final String id, final JsonNode jsonNode) {
        return configClient.createKVConfig(tenant, configName, version, id, jsonNode);
    }

    public <T> Map<String, JsonNode> createBulkKVConfig(final String tenant, final String configName, final int version, final Map<String, T> data) {
        final List<BulkConfigRequest> bulkConfigRequests = data.entrySet()
                                                               .stream()
                                                               .map(e -> new BulkConfigRequest(e.getKey(), objectMapper.convertValue(e.getValue(), JsonNode.class)))
                                                               .collect(Collectors.toList());
        return createBulkKVConfig(tenant, configName, version, bulkConfigRequests);
    }

    public Map<String, JsonNode> createBulkKVConfig(final String tenant, final String configName, final int version, final List<BulkConfigRequest> bulkConfigRequests) {
        return configClient.createBulkKVConfig(tenant, configName, version, bulkConfigRequests);
    }

    public <T> Optional<T> getKVConfig(final String tenant, final String configName, final int version, final String id, final Class<T> clazz) {
        try {
            final Optional<JsonNode> jsonNode = getKVConfig(tenant, configName, version, id);
            return jsonNode.map(node -> objectMapper.convertValue(node, clazz));
        } catch (Exception e) {
            log.error("Error", e);
            return Optional.empty();
        }
    }

    public Optional<JsonNode> getKVConfig(final String tenant, final String configName, final int version, final String id) {
        try {
            return Optional.of(kvConfigCache.get(new KVConfigKey(tenant, configName, version, id)));
        } catch (Exception e) {
            log.error("Error", e);
            return Optional.empty();
        }
    }

    @Data
    @AllArgsConstructor
    private static class StandardConfigKey {
        private final String tenant;
        private final String configName;
        private final int version;
    }

    @Data
    @AllArgsConstructor
    private static class KVConfigKey {
        private final String tenant;
        private final String configName;
        private final int version;
        private final String id;
    }

}
